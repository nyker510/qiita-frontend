// notification store module

// define state values
export const state = () => ({
  type: null,
  message: null,
  multiline: false
})

export const getters = {
  /**
   * 通知メッセージが存在しているかどうかを返す
   *
   * @param {*} state
   * @returns boolean
   */
  existMessage(state) {
    if (state.message) {
      return true
    }
    return false
  }
}

export const mutations = {
  /**
   * 通知を設定する
   * 呼び出す際には `notify/set` のように module 名を明示的に指定する
   *
   * ex).
    ```js
    this.$store.commit('notify/set', {
        type: 'default',
        message: 'foo'
      })
    ```
   *
   * @param {*} state
   * @param {{ type: string, message: string, multiline: boolean }} payload
   */
  set(state, payload) {
    // type が設定されていないとき `"default"` に置き換える
    state.type = payload.type || 'default'
    state.message = payload.message || ''
    state.multiline = payload.multiline || false
  },
  /**
   * 通知を初期化する
   *
   * @param {*} state
   */
  clear(state) {
    state.type = null
    state.message = null
    state.multiline = false
  }
}
