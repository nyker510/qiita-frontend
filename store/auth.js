// token や承認に関する情報を管理する store module
// author@nyker510
const jwtDecode = require('jwt-decode')

export const state = () => ({
  jwtToken: null
})

export const getters = {
  existToken(state) {
    if (state.jwtToken) return true
    return false
  },
  tokenObject(state) {
    const token = state.jwtToken
    if (!token) return null
    return jwtDecode(token)
  },
  authorizedType(state, getters) {
    const obj = getters.tokenObject
    if (obj === null) return null
    return obj.authorizedType
  }
}

export const mutations = {
  setToken(state, token) {
    state.jwtToken = token
  }
}

export const actions = {
  /**
   * 今持っている token が有効期限切れかどうかを判定する action
   * tokenが存在しないor有効期限が切れている時に true を返す
   */
  tokenIsExpired({ getters }) {
    const tokenObject = getters.tokenObject
    if (tokenObject === null) return true
    // token の exp は Date.now() を数値化した値の秒数なので
    // 今のミリ秒を 1000 で割って算出する
    const current = Date.now().valueOf() / 1000
    console.log(current, tokenObject['exp'])
    return current > +tokenObject['exp']
  },

  /**
   * state に保存されている token が有効かどうかを **厳密に** 確認し
   * token を更新する action
   *
   * 有効な token を state に保存できたとき true を返す
   *
   * @param {*} { state, commit, getters }
   * @returns {boolean}
   */
  async update({ state, commit, getters, dispatch }) {
    // そもそも token が存在しない場合
    console.log('exist token', getters.existToken)
    if (!getters.existToken) {
      return false
    }

    const expired = await dispatch('tokenIsExpired')
    // 有効期限切れの場合
    if (expired) {
      console.log('token is exiest but expired')
      return false
    }

    const result = await this.$axios
      .$post('/auth/token/refresh/', { token: state.jwtToken })
      .then(res => {
        console.log('success!!')
        commit('setToken', res.token)
        return true
      })
      .catch(e => {
        console.log(e.response)
        console.log('cant refresh... login again')
        commit('setToken', null)
        return false
      })
    return result
  },
  async login({ commit }, payload) {
    const response = await this.$axios
      .$post('auth/login/', payload)
      .then(res => {
        return res.data
      })
      .catch(e => {
        // console.log(e)
      })

    if (response) {
      commit('setToken', response.token)
      return 'success'
    }
  },
  async logout({ commit }) {
    commit('setToken', null)
    return true
  }
}
