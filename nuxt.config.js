require('dotenv').config()

module.exports = {
  /*
  ** Headers of the page
  */
  env: {
    appName: 'my-awesome-qiita'
  },
  head: {
    title: 'nyker-qiita',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'my-awesome-qiita' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.jpg' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/icon?family=Material+Icons'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/earlyaccess/mplus1p.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/earlyaccess/sawarabimincho.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/earlyaccess/notosansjapanese.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/webkul-micron@1.1.6/dist/css/micron.min.css',
        type: 'text/css'
      },
      {}
    ]
  },
  css: ['@/assets/css/app.styl'],
  plugins: [
    '~/plugins/local-storage.js',
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/micron.js', ssr: false },
    '~/plugins/vuetify.js',
    '~/plugins/filters.js'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  modules: ['@nuxtjs/axios', '@nuxtjs/pwa'],
  /*
  ** Build configuration
  */
  mode: 'spa',
  build: {
    vender: ['axios', 'vuetify', 'webkul-micron'],
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  workbox: {
    dev: true
  }
}
