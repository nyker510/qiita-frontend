module.exports = {
  root: true,
  extends: [
    'plugin:vue/recommended',
    "prettier"
  ],
  parserOptions: {
    sourceType: "module",
    parser: "babel-eslint",
    ecmaVersion: 8
  },
  env: {
    browser: true,
    node: true
  },
  plugins: [
    "vue",
    "prettier"
  ],
  // add your custom rules here
  "rules": {
    // allow debugger on development
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    "prettier/prettier": ["error", {
      "singleQuote": true,
      "semi": false
    } ],
  },
  globals: {}
}
