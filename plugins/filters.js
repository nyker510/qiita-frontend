import differenceInCalendarDays from 'date-fns/difference_in_calendar_days'
import differenceInCalendarMonths from 'date-fns/difference_in_calendar_months'
import Vue from 'vue'
import format from './locale-date-format'

/**
 * 現在日時からの相対的な日時を取得
 * @param {*} d 表示する日時文字列
 */
export function toRelativeDateLabel(d) {
  if (!d) return ''
  const differenceInDays = differenceInCalendarDays(new Date(), new Date(d))
  const differenceInMonths = differenceInCalendarMonths(new Date(), new Date(d))
  if (differenceInDays === 0) {
    return '今日'
  }
  if (differenceInDays === 1) {
    return '昨日'
  }
  if (differenceInDays < 7) {
    return `${differenceInDays}日前`
  }
  if (differenceInMonths === 0) {
    return '今月'
  }
  if (differenceInMonths === 1) {
    return '先月'
  }

  return differenceInMonths + 'ヶ月前'
}

/**
 * タグのパスから末尾のタグ名を取得
 * @param {*} tag タグ名
 */
export function formattedTagLabel(tag) {
  const tags = tag.split('/')
  return tags[tags.length - 2]
}

Vue.filter('toRelativeDateLabel', toRelativeDateLabel)

Vue.filter('dummyText', (v, length = 5) => {
  if (!v) return '-'.repeat(length)
  return v
})

Vue.filter('prettyDate', v => {
  return format(v, 'YYYY/MM/DD')
})

Vue.filter('prettyDateTime', v => {
  return format(v, 'YYYY/MM/DD A h:mm')
})
