var format = require('date-fns/format')
var locales = {
  ja: require('date-fns/locale/ja')
}

export default function(date, formatStr) {
  return format(date, formatStr, {
    // locale は決め打ちで
    locale: locales['ja']
  })
}
