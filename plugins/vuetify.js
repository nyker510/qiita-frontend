import Vue from 'vue'
import Vuetify from 'vuetify'
import color from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: color.blueGrey.darken3,
    secondary: color.cyan.lighten2,
    accent: color.deepOrange.lighten1
  }
})

import NotifySnackbar from '@/components/NotifySnackbar'

Vue.component('notify-snackbar', NotifySnackbar)
