export default function({ $axios, store }) {
  $axios.onRequest(config => {
    const token = store.state.auth.jwtToken
    if (!token) return

    config.headers.common['Authorization'] = 'JWT ' + token
    // 明示的に token を設定しないとアクセスしたはじめのページで authorization が必要な ajax を asyncData などで
    // 使っている場合に 401 になる

    // $axios.post('/auth/token-verify', { token: token })
    //   .then(token => {
    //     config.headers.common['Authorization'] = 'JWT ' + token
    //   })
    //   .catch(e => {
    //     const response = e.response
    //     if (!response) return
    //     console.log(response)
    //   })
  })
}
