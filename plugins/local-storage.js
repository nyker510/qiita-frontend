import createPersistedState from 'vuex-persistedstate'

export default ({ store, env }) => {
  createPersistedState({
    key: env.appName || 'awesome-app',
    paths: []
  })(store)
}
