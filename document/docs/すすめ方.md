# すすめかた

開発の進め方について

環境構築が終わったら開発に移ります。といっても何をやってよいか、どうやったらよいかわからないと思いますので開発のルールについて記述します。

## 開発のながれ

### Git Flow

開発は git-flow に則って開発をすすめていきます。

git-flow とはすべての作業を master ブランチから切り出したブランチで行い、そのブランチを master に merge するという原則に従った開発のことです。

したがって master ブランチで作業を行ってはいけません。
master は常に何かのブランチを取り込む、という形で更新します。
また通常は master ブランチから切り出した develop ブランチに対して上記のことを行い、リリースをする段階で master にマージを行います。

よって基本的には開発者は以下の流れで作業をします。 

1. `develop` ブランチから新しく作業用のブランチを作成
2. 与えられた作業を実装
3. `develop` へのマージを gitlab 上の merge request で送る
4. レビュアーが作業内容を検証, OKなら develop へ merge. そうでなければその内容をコメントし再度修正(2に戻る)

### 今回の流れ

今回は実装を勉強する、ということが主眼ですから複数人が別々の develop ブランチを持つことになります。
よって各人はまずはじめに `master` ブランチから自分の develop 用のブランチ `<username>/develop` を作りそこから更にブランチを切って作業します。

今回の実装の流れを書くと以下のようになります。

1. もし `<username>/develop` がなければ `master` ブランチから新規に作成
2. `<username>/develop` ブランチから新しく作業用のブランチを作成
3. 与えられた作業を実装
4. `<username>/develop` へのマージを gitlab 上の merge request で送る
5. レビュアーが作業内容を検証, OKなら `<username>/develop` へ merge. そうでなければその内容をコメントし再度修正(2に戻る)

> 例 username = `"nyk"` の場合

```bash
# nyk という名前のブランチが無いことを確認
18:45:48 in qiita-frontend on  master [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git br -a | grep nyk

# 今は master branch にいます
18:45:48 in qiita-frontend on  master [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git br
  develop
  manager/issue-8
* master

# `ch -b` は create branch and checkout を同時にやるオプション
18:46:25 in qiita-frontend on  master [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git ch -b nyk/develop
Switched to a new branch 'nyk/develop'

# `nyk/issue-1` という名前で作業用ブランチを作成
18:46:34 in qiita-frontend on  nyk/develop [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git ch -b nyk/issue-1
Switched to a new branch 'nyk/issue-1'

# 適当な作業をして
18:46:48 in qiita-frontend on  nyk/issue-1 [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ touch hoge.md

18:46:59 in qiita-frontend on  nyk/issue-1 [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git add hoge.md

18:47:04 in qiita-frontend on  nyk/issue-1 [!+?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git st
ブランチ nyk/issue-1
コミット予定の変更点:
  (use "git reset HEAD <file>..." to unstage)

        new file:   hoge.md

# コミットして作業を保存
18:47:06 in qiita-frontend on  nyk/issue-1 [!+?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git cm -m"[add] hoge readme"
[nyk/issue-1 1d4944b] [add] hoge readme
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 hoge.md

# remote branch へ push
18:47:16 in qiita-frontend on  nyk/issue-1 [!?] is 📦 v1.0.0 via ⬢ v8.11.2
➜ git push -u
Delta compression using up to 8 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 279 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for nyk/issue-1, visit:
remote:   https://gitlab.com/nyker510/qiita-frontend/merge_requests/new?merge_request%5Bsource_branch%5D=nyk%2Ffeature-1
remote:
To git@gitlab.com:nyker510/qiita-frontend.git
 * [new branch]      nyk/issue-1 -> nyk/issue-1
Branch nyk/issue-1 set up to track remote branch nyk/issue-1 from origin
```

## 何をやればいいの?

やらなくてはならないこと、はすべて issue で管理します。

> https://gitlab.com/nyker510/qiita-frontend/issues

この中にある issue を一つづつこなしていくことが貴方がやるべきタスクです。
実際の開発では

* PM が実装すべき内容やタスク等を issue として切り出し
* 誰がやるのかをアサイン
* 担当者が実装をする

という流れになります。

### Issue の例

例えば[#3 投稿に tag を紐づけられるようにする](https://gitlab.com/nyker510/qiita-frontend/issues/3) を見てみましょう。
issue には以下のような文章が書かれていると思います。

```markdown
## 何を作るのか？or 何が問題なのか？(必須)

投稿ページではタグを入力する input 要素があります。
この input 要素に対してタグ入力を実装してください。

### 要件詳細

* input 要素内で自由に入力できる
* タグ候補は api から ajax で取得
* 入力にマッチしたタグがフォーム下部にサジェストされる
* サジェストされるdomを押す or スペースを入力するとそれまで入力されていた部分がタグとしてフォームに追加される
* タグ候補にない場合 api と疎通して新しく tag を作成
* 追加されたタグには `x` ボタンがあり, 押すとタグを消去することができる

## ブランチの切り方

* source: `<user-name>/develop`
* name: `<user-name>/issue-<issue-number>`
* target: `<user-name>/develop`

## レビュアー

@nyker510
```

基本的に issue は 何を作るか、その詳細、ブランチの切り方、レビュアー が記述されています。

何を作るのか、その詳細、に関してはそのままで、この issue で達成すべき内容が書かれています。
ここに書いてあることだけじゃわからない、ということがあれば issue 上でコメントなどを行って確認をとってください。

ブランチの切り方には

* 作業用ブランチを作成する元のブランチ (source)
* 作業用ブランチ名 (name)
* マージリクエストを送る先のブランチ名 (target)

が指定されます。

基本的に作業用のブランチは `<username>/issue-<issue番号>` です。例えば nyk が issue 番号 1 を作業するときは `nyk/issue-1` となります。
作業が終わりマージリクエストを送るときにはレビュアーに指定されている人を assign します。

!!!tips
    このときに slack かなにかでレビュー御願いしますと言ってもらえるとすぐ見てもらえるかも知れません

## 作業がおわったら何をするの?

作業が終了したらまずリモートリポジトリに push してください。そして issue に記述されている target ブランチに対して gitlab 上でマージリクエストを作成します。

マージリクエストは以下のフォーマットにしたがって作成してください。[^1]

```markdown
## 概要

## 関連する Issue

## 何を実装しているか

## 難しい実装点はあるか

## テストの方法

## 動作のスクリーンショット
```

### 注意点

PR を作成後には必ず

1. target ブランチへのマージがコンフリクトしないかどうかの確認
2. コンフリクトする場合には target から作業ブランチへのマージを行ってコンフリクトを解消後にPRを出す

を行ってください。

[^1]: 厳密にしたがう必要はありませんが情報としてフォーマットに書かれた内容が誰にでも伝わるように記述してね。
