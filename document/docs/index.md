# Qiita Frontend

ようこそ！ Qiita のフロント実装へ!

このリポジトリは Qiita のようなサービスのフロント側を作成することで Vue.js および node.js などに慣れるための開発練習うことを目的としています。

## Quick Start

```bash
cp sample.env .env
docker-compose build
docker-compose up -d
docker exec -it qiita-node-app sh

yarn
yarn dev
```
