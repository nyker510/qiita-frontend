# Docker について

このプロジェクトの docker-compose の定義は以下のようになっています。
`services` 内にそれぞれ `node` と `docs` と名前のついた container とその定義が記述されています。


```yml
version: '2.3'
services:
  node:
    build: ./docker/node
    tty: true
    container_name: "qiita-node-app"
    working_dir: /home/node/app
    volumes:
      - ./:/home/node/app
    ports:
      - "3303:3000"
  docs:
    build:
      dockerfile: ${PWD}/docker/mkdocs/Dockerfile
      context: ./document
    container_name: "qiita-mkdocs"
    ports:
      - "4545:8000"
    working_dir: /home/docs/app
    volumes:
      - ./document/:/home/docs/app
```
