/**
 * store に token が保存されていない場合に
 * ログインページにリダイレクトする
 */
export default async function({ store, redirect }) {
  const isExpired = await store.dispatch('auth/tokenIsExpired')
  console.log('token is expired: ', isExpired)
  if (isExpired) {
    store.commit('auth/setToken', null)
    redirect('/login')
  }
}
